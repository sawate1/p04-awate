#ifndef KRUSKALS_H_
#define KRUSKALS_H_
#include<stdio.h>
#include<stdlib.h>

struct edge{
	int from,to,cost;
};

void union3(int repx, int repy, int height[12], int Set[12]);
int find3(int x, int V[12]);

void sort(struct edge e[12], int edgeCount);
void printEdge(struct edge e);
	
#endif