#include "kruskals.h"
//#include <stdio.h>

void kruskals(int **matrix,int x);

void printEdge(struct edge e){
	/*printf("\nFrom %d ",e.from);
	printf("to %d",e.to);
	printf(" => cost %d",e.cost);*/
	printf("%d - %d -> %d\n",e.to+1,e.from+1,e.cost);
}

void sort(struct edge *e, int edgeCount){
	int i=0,j=0;
	int temp=0;

    printf("edgecount %d",edgeCount);	
	for(i=0;i<edgeCount;i++){
		printEdge(e[i]);
	}
	
	
	for (i = 0; i < edgeCount; i++) {     
		for (j = i+1; j < edgeCount; j++) {
			if (e[i].cost > e[j].cost){
				//swap(&e[j], &e[j+1]);
				temp=e[i].from;
				e[i].from=e[j].from;
				e[j].from=temp;
				
				temp=e[i].to;
				e[i].to=e[j].to;
				e[j].to=temp;
				
				temp=e[i].cost;
				e[i].cost=e[j].cost;
				e[j].cost=temp;
			}
		}
	}
   printf("\nsorted edges\n");	
	for(i=0;i<edgeCount;i++){
		printEdge(e[i]);
	}

}

int find3(int x, int set[12]){
	//find root	of	tree with x
	int root = x;
	int node=0;
	int parent=0;
	while	(root != set[root]){
	  root =set[root];
	}
	//compress	 path	from	x	to	root
	node = x;
	while (node != root){
		parent = set[node];
		set[node]	= root;	//	node points	to	root
		node= parent;
	}
	return	root;
}

void union3(int repx, int repy, int height[12], int Set[12]){	
	if	(height[repx]	==	height	[repy]){
		height[repx]++;
		Set[repy]= repx;//y’s	tree	points	to	x’s	tree	
	}
	else{
		if	(height[repx]	> height	[repy]){
		    Set[repy]	= repx ;//y’s	tree	points	to	x’s	tree	
		}
		else{
		    Set[repx]	= repy; //x’s	tree	points	y’s	tree
		}
	}
}
struct edge createEdge(int fromIn, int toIn, int costIn){
	struct edge createdEdge;
	createdEdge.from=fromIn;
	createdEdge.to=toIn;
	createdEdge.cost=costIn;
	
	return createdEdge;
}

void kruskals(int **a,int n)
{
	//struct edge e[n];
	struct edge *e=malloc(sizeof(struct edge)*(n*(n-1)));
	
	int i=0,j=0;
	int edgeCount=0;
	for(i=0;i<6;i++){
		e[i].cost=0;e[i].to=0;e[i].from=0;
	}
	int u=0,v=0;
	int ucomp=0,vcomp=0;
	for(i=0;i<n;i++){
		for(j=i+1;j<n;j++){
			if(i<j){
				e[edgeCount].from=i;
				e[edgeCount].to = j;
				e[edgeCount].cost=a[i][j];
				edgeCount++;
			}
		}
	}
	sort(e,edgeCount);
	
	struct edge T[12];
	int V[12];
	int heights[12];
	int tCount=0;
	for(i=0;i<n;i++){
		V[i]=i;
		heights[i]=0;
	}
	for(i=0;i<edgeCount;i++){
		u=e[i].from;
		v=e[i].to;
		ucomp=find3(u,V);
		vcomp=find3(v,V);
		if(ucomp!=vcomp){
			T[tCount++]=createEdge(u,v,e[i].cost);
			union3(ucomp,vcomp,heights,V);
		}
		if(tCount==(n-1)) break;
	}
	printf("\nPrinting result\n edgeCount: %d \n",edgeCount);
	printf("\nPrinting result\n tCount: %d \n",tCount);
	int totalCost = 0;
	for(i=0;i<tCount;i++){
		totalCost += T[i].cost;
		//printf("\n%d - %d -> %d",T[i].from,T[i].to,T[i].cost);
		printEdge(T[i]);
	}
	printf("\ntotal cost %d \n",totalCost);
}


int main()
{
	int i,n;
	char buffer[65535];
	int i, m, n, temp;
	int j;int k=0;
	//File input handler.
	FILE *fp = fopen( argv[1], "r" );
	int token[10000];

        if(argc < 2)
		{
			printf("Error Please put the input text file name\n");
			return 0;
		}

		int z=0;
        

		//reading the number of nodes and adjacency matrix from the input file
		while((m=fread(buffer,1,65535,fp))>0)
		{
			i=0;
			while(i < m){
			
			
                if(buffer[i]== '\n' || buffer[i]==',' || buffer[i]==' ')
                {	
                      token[k++]=temp;
                      temp=0;
                      if(buffer[i]== '\n') z++;
			
                }
                else
                {
                    temp=temp*10+(buffer[i]-48);
                }
				i++;
			} 

		printf("Number of nodes in the graph : %d\n",z+1);
	} 
	
//initialization of the graph
		token[0]=0;
		n = z+1;
		k = 0;
		

        int matrix[n][n] ;
	for(i=1; i<=n; i++)
	{
		for(j=1; j<=n; j++)
		{
			matrix[i][j] = token[k++];
		}
	}
	printf("\nThe adjacency matrix is:\n");
	for(i=1; i<=n; i++)
	{
		for(j=1; j<=n; j++)
		{
			printf("%d\t",matrix[i][j]);
		}
		printf("\n");
	}
	int **a = malloc(sizeof(int*)*n);
		for(i=0;i<n;i++){
			a[i]=malloc(sizeof(int)*n);
			//a[i]=malloc(sizeof(int));
		}


return 0;
}