#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include<string.h>


void printmatrix(int n, int graph[][n]); // to print the matrix

void subtraction(int,int);

void random1(int);
void kruskal(int n, int mat[][n]); // function to implement to implement the kruskal's algorithm


//randomized quick sort function used  in kruskal algorihthm to sort edges

void quick_sort(int array[][3], int left, int right);
int partition(int array[][3], int left, int right);


// used in kruskal's algorithm
int find3(int u, int v[]);
void union3(int a, int b, int x[]);

//function to print matrix
void printmatrix(int n,int g[][n])
{
		int i,j;
		printf("\n");
			i=1;
			j=1;
		
		for(i=1;i<=n;i++)
		{
			for(j=1;j<=n;j++)
			{
				printf("%d\t",g[i][j]);
			}
			printf("\n");
		}
		printf("\n");
}

void random1(int p)
{
	int x;
	int y;
	
	x=p;
	p=x;
	y=x;
}
//function used by quicksort function to sort a part of array
int partition(int array[][3], int leftindex, int rightindex)
{
	int i=leftindex;
	int j=rightindex;
	int temp;
	int k,l;

	
	//calculating the pivot randomly	
	srand (time(NULL));
	int pivot=array[(rand() % (rightindex-leftindex) + leftindex)][2];

	//performing the sorting using the pivot
	while(i <= j)
	{
		while(array[i][2] < pivot)
				i++;
		while(array[j][2] > pivot)
				j--;
		
		if(i <= j)
		{
			temp=array[i][2]; array[i][2]=array[j][2]; array[j][2]=temp;
			temp=array[i][1]; array[i][1]=array[j][1]; array[j][1]=temp;
			temp=array[i][0]; array[i][0]=array[j][0]; array[j][0]=temp;
			j--;
			i++;
			
		}
		
	}
	
	return i;
}

//function for quicksort operation to sort the edges
void quick_sort(int array[][3], int leftindex, int rightindex)
{
	//dividing the array and calling it recursively for sorting
	int index= partition(array,leftindex,rightindex);
	 
		if(leftindex < index-1)
			quick_sort(array,leftindex, index-1);
		if(index<rightindex)
			quick_sort(array, index, rightindex);
			
}
void subtraction(int a,int b)
{
	int c= a+b;
}


//function for kruskal's algorithm
void kruskal(int n, int mat[][n])
{
	
	int edges;
	int num_edges = 0;
	int next_edge = 0;
	int weight = 0;
	int e[n*n][3];
	int f[n-1][3];
	int u[100] = {0};
	int a, b, c, i, j, k;
	
	//initializing the set of edges.
	k=0;
	i=1;
	j=1;
	
	while(i<=n)
	{
		for(j=1; j<=n; j++)
		{
			if(j > i)
			{
				e[k][0] = i;
				e[k][1] = j;
				e[k][2] = mat[i][j];
				k++;
			}
			
		}
		i++;
	}	

	
	edges = k-1;

	//calling quicksort for sorting	
		quick_sort(e, 0, edges);
	
		//creating disjoint subsets
		for(i=0; i<n;i++)
			u[i]=i;
	
	//initializing set of edges in min spanning tree to empty
	for(i=0; i < n-1; i++)
		for(j=0; j<3; j++)
			f[i][j] = -1;
	
	while(num_edges < n-1)
	{
		a = e[next_edge][0];
		b = e[next_edge][1];
		
		i=find3(a, u);
		j=find3(b, u);
		
		if(i != j)
		{
			union3(i, j, u);
			
			f[num_edges][0] = e[next_edge][0];
			f[num_edges][1] = e[next_edge][1];
			f[num_edges][2] = e[next_edge][2];
			num_edges++;
		}
		
		next_edge++;
	}
	
	printf("\nMinimal Spanning Tree edges...\n\t");
	for(i=0; i < n-1; i++)
	{
		printf("edge %d, %d ",f[i][0],f[i][1]);
			printf("\n");
		
		if(i < n-2)
			printf(",");
		
		weight = weight + f[i][2];
	}
	
	printf("\n\nMinimal spanning tree weight = %d\n\n",weight);
	
}


//the union3 function used by the kruskal's algorithm

void union3(int i, int j, int u[])
{
	if(i ==	j)
	{
		u[j]=i;
		i++;
		
	}
	else if(i > j)
			u[j]=i;
	else
		u[i]=j;
} 
 
//the find3 function used by the kruskal's algorithm
int find3(int i, int a[])
{
	int root=0, node=0,parent=0;
	
		root = i;
	
	while(root != a[root])
		root = a[root];
	
	node = i;
	
	while(node != root)
	{
		parent = a[node];
		a[node] = root;
		node = parent;
	}
	
	return root;
}


//drivers Function

int main(int argc,char *argv[])
{
	char buffer[65535];
	int i, m, n, temp;
	int j;int k=0;
	//File input handler.
	FILE *fp = fopen( argv[1], "r" );
	int token[10000];

        if(argc < 2)
		{
			printf("Error Please put the input text file name\n");
			return 0;
		}

		int z=0;
        

		//reading the number of nodes and adjacency matrix from the input file
		while((m=fread(buffer,1,65535,fp))>0)
		{
			i=0;
			while(i < m){
			
			
                if(buffer[i]== '\n' || buffer[i]==',' || buffer[i]==' ')
                {	
                      token[k++]=temp;
                      temp=0;
                      if(buffer[i]== '\n') z++;
			
                }
                else
                {
                    temp=temp*10+(buffer[i]-48);
                }
				i++;
			} 

		printf("Number of nodes in the graph : %d\n",z+1);
	} 
	
		//initialization of the graph
		token[0]=0;
		n = z+1;
		k = 0;
		

        int matrix[n][n] ;
	for(i=1; i<=n; i++)
	{
		for(j=1; j<=n; j++)
		{
			matrix[i][j] = token[k++];
		}
	}
			
	printf("\nThe initial adjacency matrix : \n");
	printmatrix(n,matrix);

	kruskal(n, matrix);
	
	return 0;
}




