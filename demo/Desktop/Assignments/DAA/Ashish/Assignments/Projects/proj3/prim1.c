#include <stdio.h>
#include <limits.h>
#include <stdlib.h>


int visited[100];

void primMST(int graph[100][100],int N);

struct edge 
{
	int weight;
	int u, v;   // edge from u ---> v
};

struct edge *heap;
int size;

void Init()
{
        size = 0;
        heap[0].weight = -INT_MAX;
}
void print(int s)
{
	int i;
	for(i=1;i<=s;i++)
	printf("heap[i].weight=%d ",heap[i].weight);
}

/*Insert an element into the heap */
void Insert(struct edge element)
{
        size++;
        heap[size] = element; /*Insert in the last place*/
        /*Adjust its position*/
        int now = size;
        while(heap[now/2].weight > element.weight) 
        {
                heap[now] = heap[now/2];
                now /= 2;
        }
        heap[now] = element;
}
struct edge DeleteMin()
{
        
        struct edge minElement,lastElement;
		int child,now;
	
        minElement = heap[1];
        lastElement = heap[size--];
        /* now refers to the index at which we are now */
        for(now = 1; now*2 <= size ;now = child)
        {
                /* child is the index of the element which is minimum among both the children */ 
                /* Indexes of children are i*2 and i*2 + 1*/
                child = now*2;
                /*child!=heapSize beacuse heap[heapSize+1] does not exist, which means it has only one 
                  child */
                if(child != size && heap[child+1].weight < heap[child].weight ) 
                {
                        child++;
                }
                /* To check if the last element fits ot not it suffices to check if the last element
                   is less than the minimum element among both the children*/
                if(lastElement.weight > heap[child].weight)
                 {
                        heap[now] = heap[child];
                }
                else /* It fits there */
                {
                        break;
                }
        }
        heap[now] = lastElement;
        return minElement;
}


int main(int argc,char *argv[])
{
	
	int N,c;
	int graph[100][100];
	int k,i,j;
	FILE *file;
	size_t buffer=0;
	char *to;
	char *word=NULL;
	const char s[2]=", ";
	file=fopen(argv[1],"r");
	if(!file)
		printf("file is not present");
	i=0;
	k=0;
	while(!feof(file))
	{
		getline(&word,&buffer,file);
		to=strtok(word,s);
		j=0;
		while(to!=NULL)
		{
			graph[i][j]=atoi(to);
			j++;
			to=strtok(NULL,s);
		}
		i++;
		k++;
	}
	printf("\n");
	
	N=k-1;
	
	printf("no. of nodes = %d \n",N);
	printf("Adjacency Matrix: \n");
	i=0;
	while(i<N)
	{
		j=0;
		while(j<N)
		{
			printf("%d ",graph[i][j]);
			j++;
		}
		printf("\n");
		i++;
	}	/*int graph[V][V] = {{0,2,6,999},
                     {2,0,5,7},
                      {6,5,0,999},
                      {999,7,999,0},
                     }; */
					 // Print the solution*/
					 
					 
	primMST(graph,N);

    return 0;
}


// A utility function to print the constructed MST stored in parent[]
int printMST(int parent[], int n, int graph[100][100])
{
	int i;
	printf("Edge   Weight\n");
	for ( i = 1; i< n; i++) 
	printf("%d - %d    %d \n", parent[i]+1, i+1, graph[i][parent[i]]);
}

// Function to construct and print MST for a graph represented using adjacency
// matrix representation
void primMST(int graph[100][100],int V)
{

	int count,i,v;
	int parent[V]; // Array to store constructed MST
    int key[V];   // Key values used to pick minimum weight edge in cut
    int visited[V];  // To represent set of vertices not yet included in MST
	 
	struct edge temp,dummy;
	//struct edge * PriorityQueue= malloc(sizeof(struct edge) * 100);
	heap = malloc(sizeof(struct edge) * 1000);
     // Initialize all keys as INFINITE
     for ( i = 0; i< V; i++)
        key[i] = INT_MAX, visited[i] = 0;

     // Always include first 1st vertex in MST.
	key[0] = 0;     // Make key 0 so that this vertex is picked as first vertex
	parent[0] = -1; // First node is always root of MST 
	visited[0] = 1;
	Init();
	
    for(i=0;i<V;i++)
	{  
		if(graph[0][i] && graph[0][i]!=999 && visited[i] == 0)
		{
			temp.weight=graph[0][i];
			temp.u=0;
			temp.v=i;
			Insert(temp);
			
		}
    }	
		print(size);

	// The MST will have V vertices
     for (count = 0; count < V-1; count++)
     {
        // Pick thd minimum key vertex from the set of vertices
        // not yet included in MST
			//printf("\n size of queue %d ",size);
        
			temp=DeleteMin();
			//printf("\n min weight %d ",temp.weight);
		
			//size=0;
		
			int x=temp.u;
			int u= temp.v;
			visited[u] = 1;
			parent[u] = x;	
			for(i=0;i<V;i++)
			{ 
				if(graph[u][i] && graph[0][i]!=999 && visited[i] == 0)
				{
					temp.weight=graph[u][i];
					temp.u=u;
					temp.v=i;
					Insert(temp);
				}
			}
			
			//u=temp.u;
			printf("\n");
			//print(size);

        // Add the picked vertex to the MST Set
		//	visited[u] = true;

        // Update key value and parent index of the adjacent vertices of
        // the picked vertex. Consider only those vertices which are not yet
        // included in MST
			for ( v = 0; v < V; v++)
			{	
           // graph[u][v] is non zero only for adjacent vertices of m
           // visited[v] is false for vertices not yet included in MST
           // Update the key only if graph[u][v] is smaller than key[v]
				if (graph[u][v] && visited[v] == 0 && graph[0][i]!=999 && graph[u][v] <  key[v])
					parent[v]  = u, key[v] = graph[u][v];
			}
     }

     // print the constructed MST
printMST(parent, V, graph);
}
