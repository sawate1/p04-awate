#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


//function definitions
int** allocate(int);
void printMatrix(int,int **);
void tromino_tiling(int,int **,int,int);

int num=1;//to maintain count to print tile number on matrix


//Function to allocate and initialize the matrix
int** allocate(int n)
{
	int i,j;
	int **matrix = malloc(n * sizeof(int *));
	
	for(i = 0; i<= n;i++)
		matrix[i]=malloc(n * sizeof(int));
		
	for(i = 0; i<= n ;i++)
	{

		for(j=0; j<=n ;j++)
		{
			matrix[i][j] = 0;
		}
	}	
	return matrix;
}



//driver function
int main(int argc,char *argv[])
{
    
    	int n,k,x_missing,y_missing;
    	int i,j;
	srand(time(NULL));
     	
        
	if(argc!=2)
	{
		printf("Format to run: ./tro k ,  k should be any positive integer\n");
		return 0;
	}
	k = atoi(argv[1]);
        if (k < 1) 
	{ 
		fprintf(stderr,"ALERT !!!! Please Enter a positive integer \n"); 
			return -1;
	}

         n =pow(2,k); 
	  //To randomly create a hole in the matrix	
	 x_missing= rand() % n; 
         y_missing= rand() % n;

    	printf("Size of matrix n = 2^k = %d\n",n);
       	int **a = allocate(n);

	//initializing the matrix
	i=0;
	while(i<n)
	{
		j=0;
		while(j<n)
		{
			a[i][j]=0;
		        j++;
		}
		i++;
	}
    	
    
    	a[x_missing][y_missing]=-2;

        printf("\nPuzzle Matrix\n");
        printMatrix(n,a);

   	 tromino_tiling(n,a,0,0);

    	printf("\nSolved Puzzle !!\n");
    	printMatrix(n,a);

    return 1;
}

//Function to  print the matrix
void printMatrix(int n,int **a)
{
    int i,j;

    for(i=0; i<n; i++)
    {
        for(j=0; j<n; j++)
        {
            if(a[i][j]==-2)
                printf("X\t");
            else
                printf("%d\t",a[i][j]);
        }

        printf("\n");
    }

    printf("\n");
} 

void tromino_tiling(int n,int **b,int x_board,int y_board)
{
    int missing_x=0, missing_y=0;

    int i,j;

    if(n==2) //base condition
    {
	i = 0;
	while(i<n)
	{
		j = 0;
		while(j < n)
		{
			if(b[x_board+i][y_board+j] == 0)
                               b[x_board+i][y_board+j]=num;
		   j++;
			
		}
		i++;
	}         
	
       num++;
      return;
    }

//finding the missing hole
    missing_x=x_board;
    missing_y=y_board;
        i=x_board;
	while(i < x_board+n)
	{
		j=y_board;
		while(j< y_board+n)
		{
			 if(b[i][j]!= 0)
            		 {
                		missing_x = i;
                		missing_y = j;
            		 }
		  j++;	
		}
    		i++;
	}	
    
//Hole is present in first quadrant of matrix
    if(missing_x < x_board+n/2 && missing_y < y_board+n/2)
    {
        tromino_tiling(n/2,b,x_board,y_board);

        b[x_board+n/2][y_board+n/2-1] = num;
        b[x_board+n/2][y_board+n/2] = num;
        b[x_board+n/2-1][y_board+n/2] = num;
        
	num++;
        
	tromino_tiling(n/2,b,x_board,y_board+n/2);
        tromino_tiling(n/2,b, x_board+n/2, y_board);
        tromino_tiling(n/2,b, x_board+n/2, y_board+n/2);

    }
//Hole is present in 2nd quadrant of matrix
    else if(missing_x < x_board+ n/2 && missing_y >= y_board+n/2)
    {
        tromino_tiling(n/2,b, x_board, y_board+n/2);

        b[x_board+n/2][y_board+n/2-1] = num;
        b[x_board+n/2][y_board+n/2] = num;
        b[x_board+n/2-1][y_board+n/2-1] = num;
        num++;
        tromino_tiling(n/2,b, x_board, y_board);
        tromino_tiling(n/2,b, x_board+n/2,y_board);
        tromino_tiling(n/2,b, x_board+n/2, y_board+n/2);

    }
//Hole is present in 3rd quadrant of matrix
    else if(missing_x >= x_board+n/2 && missing_y < y_board+n/2)
    {
        tromino_tiling(n/2,b, x_board+n/2, y_board);

        b[x_board+n/2-1][y_board+n/2] = num;
        b[x_board+n/2][y_board+n/2] = num;
        b[x_board+n/2-1][y_board+n/2-1] = num;
        num++;
        tromino_tiling(n/2,b, x_board, y_board);
        tromino_tiling(n/2,b, x_board, y_board+n/2);
        tromino_tiling(n/2,b, x_board+n/2, y_board+n/2);

    }
//Hole is present in 4th quadrant of matrix
    else
    {
        tromino_tiling(n/2,b, x_board+n/2, y_board+n/2);

        b[x_board+n/2-1][y_board+n/2] = num;
        b[x_board+n/2][y_board+n/2-1] = num;
        b[x_board+n/2-1][y_board+n/2-1] = num;
        num++;
        tromino_tiling(n/2,b, x_board+n/2, y_board);
        tromino_tiling(n/2,b, x_board, y_board+n/2);
        tromino_tiling(n/2,b, x_board, y_board);

    }

    return;
} 







