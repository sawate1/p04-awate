Ashish Pateria
B00638413



Steps to implement below files:

Make file contents:
all: stra tro floyd

stra: pateria_pa2_stra.c
	gcc pateria_pa2_stra.c -o stra
tro:  pateria_pa2_tro.c
	gcc pateria_pa2_tro.c -lm -o tro
floyd: pateria_pa2_floyd.c
	gcc pateria_pa2_floyd.c -o floyd

clean:
	rm stra tro floyd


1:pateria_pa2_stra.c
	make stra
	./stra 4 where 4 is the size of matrix.
	./stra to check how to give in put via command line

2:pateria_pa2_tro.c
	make tro
	./tro
Format to run: ./tro k ,  k should be any positive integer

this will give how to implement ./tro
./tro 4
Size of matrix n = 2^k = 16

will give output.
./tro 2 etc.

3:pateria_pa2_floyd.c
	make floyd
./floyd
enter the number of vertices: 5
where 5 is the number of vertices of the graph in the input file //explicit






