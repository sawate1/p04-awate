#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int** allocate(int);
int** createMatrix(int);
void printMatrix(int**, int);
int** multiply(int**, int**,int);
int** addmat(int**,int**,int);
void strassen_multiply(int**,int**,int**,int);
void terminatecondition(int**,int**,int**);
void submatrix(int**,int**,int,int,int);

//Function to allocate space to matrix
int** allocate(int size)
{	
	int i,j;
	int **a= malloc(sizeof(int*) * size);

	for(i=0;i<size;i++)
	{
		a[i]=malloc(sizeof(int) * size);
	}
	for(i=0;i<size;i++)
	{
		for(j=0;j<size;j++)
		{
			a[i][j]=0;
		}
	}
	return a;
	
}

//Function to create matrix
int** createMatrix(int size)
{
	
	int i,j;
	int **matrix=allocate(size);
	for(i=0;i<size;i++)
	{
		for(j=0;j<size;j++)
		{
			matrix[i][j]= rand()%21 +(-10);
		}
	}
	
	return matrix; 
}
//Function to print matrix 
void printMatrix(int ** matrix,int size)
{
	int i,j;	
	printf("The matrix is:\n");
	for(i=0;i<size;i++)
	{
		for(j=0;j<size;j++)
		{
			printf("%d ",matrix[i][j]);
		}
		printf("\n");
	}
	
}

//Function for normal matrix multiplication
int** multiply(int **mat1,int **mat2,int size)
{
	int i,j,k;	
	int **result=allocate(size);
	for(i=0;i<size;i++)
	{
		for(j=0;j<size;j++)
		{
			result[i][j] = 0;
            		for (int k = 0; k < size; k++)
            		{
                		result[i][j] += mat1[i][k]*mat2[k][j];
            		}	
		}
	}
return result;
	
}

//Function for addition of two matrices
int** addmat(int** matrix1,int** matrix2,int size)
{
	int i,j,k;	
	int **add=allocate(size);
	for (int i = 0; i < size; i++)
    		{
        		for (int j = 0; j < size; j++)
        		{
            			
            		
                			add[i][j] = matrix1[i][j]+matrix2[i][j];
            			
        		}
    		}
return add;
	
}

//Function for subraction of two matrices
int** submat(int** mat1,int** mat2,int size)
{
	int i,j,k;	
	int **sub=allocate(size);
	for (int i = 0; i < size; i++)
    		{
        		for (int j = 0; j < size; j++)
        		{
            			
                			sub[i][j] = mat1[i][j] - mat2[i][j];
            			
        		}
    		}
return sub;
	
}

//Function for termination conditon of recursion
void terminatecondition(int** mat1,int** mat2,int** res)
{
	int m1,m2,m3,m4,m5,m6,m7;

	m1 = (mat1[0][0] + mat1[1][1]) * (mat2[0][0] + mat2[1][1]);
	m2 = (mat1[1][0] + mat1[1][1]) * mat2[0][0];
	m3 = mat1[0][0] * (mat2[0][1] - mat2[1][1]);
	m4 = mat1[1][1] * (mat2[1][0] - mat2[0][0]);
	m5 = (mat1[0][0] + mat1[0][1]) * mat2[1][1];
	m6 = (mat1[1][0] - mat1[0][0]) * (mat2[0][0] + mat2[0][1]);
	m7 = (mat1[0][1] - mat1[1][1]) * (mat2[1][0] + mat2[1][1]);

	res[0][0] = m1+m4-m5+m7;
	res[0][1] = m3+m5;
	res[1][0] = m2+m4;
	res[1][1] = m1+m3-m2+m6;

}

//Function for division of matrix into subparts
void submatrix(int** child,int** parent,int n1,int n2,int size)
{
	int p,q;
	int i_temp = n1;
	int j_temp;
	for(p = 0;p<size;p++)
	{
		j_temp = n2;
		for(q = 0;q<size;q++)
		{
			child[p][q] = parent[i_temp][j_temp];
			j_temp++;
		}
		i_temp++;
	}
}

//Function to create matrix from sub matrices
void compose(int size,int **c1,int **c2,int **c3,int **c4,int **res)
{
	int i,j,p,q;
	for(i=0,p=0;i<size/2;i++,p++)
	for(j=0,q=0;j<size/2;j++,q++)
		res[i][j] = c1[p][q];

	for(i=0,p=0;i<size/2;i++,p++)
	for(j=size/2,q=0;j<size;j++,q++)
		res[i][j] = c2[p][q];

	for(i=size/2,p=0;i<size;i++,p++)
	for(j=0,q=0;j<size/2;j++,q++)
		res[i][j] = c3[p][q];

	for(i=size/2,p=0;i<size;i++,p++)
	for(j=size/2,q=0;j<size;j++,q++)
		res[i][j] = c4[p][q];
}

//Function for strassen matrix Multiplication
void strassen_multiply(int** mat1,int** mat2,int** res,int size)
{
	
	
	if(size == 2)
	{
		terminatecondition(mat1,mat2,res);
	return;
	}

	int **mat1_11 = allocate(size/2);
	int **mat1_12 = allocate(size/2);
	int **mat1_21 = allocate(size/2);
	int **mat1_22 = allocate(size/2);

	int **mat2_11 = allocate(size/2);
	int **mat2_12 = allocate(size/2);
	int **mat2_21 = allocate(size/2);
	int **mat2_22 = allocate(size/2);

	//creation of sub-matrices
	submatrix(mat1_11,mat1,0,0,size/2);
	
	submatrix(mat1_12,mat1,0,size/2,size/2);
	

	submatrix(mat1_21,mat1,size/2,0,size/2);
	submatrix(mat1_22,mat1,size/2,size/2,size/2);

	submatrix(mat2_11,mat2,0,0,size/2);
	submatrix(mat2_12,mat2,0,size/2,size/2);
	submatrix(mat2_21,mat2,size/2,0,size/2);
	submatrix(mat2_22,mat2,size/2,size/2,size/2);
	
	//implentation strassen algorithm on each sub matrices
	int** m1 = allocate(size/2);
	strassen_multiply(addmat(mat1_11,mat1_22,size/2),addmat(mat2_11,mat2_22,size/2),m1,size/2);

	int** m2 = allocate(size/2);
	strassen_multiply(addmat(mat1_21,mat1_22,size/2),mat2_11,m2,size/2);

	int** m3 = allocate(size/2);
	strassen_multiply(mat1_11,submat(mat2_12,mat2_22,size/2),m3,size/2);
	
	int** m4 = allocate(size/2);
	strassen_multiply(mat1_22,submat(mat2_21,mat2_11,size/2),m4,size/2);
	
	int** m5 = allocate(size/2);
	strassen_multiply(addmat(mat1_11,mat1_12,size/2),mat2_22,m5,size/2);

   
	int** m6 = allocate(size/2);
	strassen_multiply(submat(mat1_21,mat1_11,size/2),addmat(mat2_11,mat2_12,size/2),m6,size/2);


	int** m7 = allocate(size/2);
	strassen_multiply(submat(mat1_12,mat1_22,size/2),addmat(mat2_21,mat2_22,size/2),m7,size/2);

	//compose
	int **c1=addmat(submat(addmat(m1,m4,size/2),m5,size/2),m7,size/2);

	int **c2=addmat(m3,m5,size/2);
	int **c3=addmat(m2,m4,size/2);
	int **c4=addmat(submat(addmat(m1,m3,size/2),m2,size/2),m6,size/2);
	compose(size,c1,c2,c3,c4,res);

	
		
	
}
 

//driver function
int main(int argc,char *argv[])
{	
	
	int **multresult;
	//validations
	if(argc!=2)
	{
		printf("Format to run: ./srta <size of matrix> , should be power of 2\n");
		return 0;
	}
	int inputSize = atoi(argv[1]);
	if(inputSize<2)
	{
		printf("Enter size in nonzero power of 2\n");
		return 0;
	}
	float temp = (float)inputSize;
	while(temp != 1.0)
	{
		temp = temp/2;
		if(temp < 1.0)
		{
			printf("Enter size in power of 2\n");
			return 0;
		}
	}	
	
		
	srand(time(NULL));


	int **mat1 = createMatrix(inputSize);
	int **mat2 = createMatrix(inputSize);
	
	printf("\nFirst martrix:\n");	
	printMatrix(mat1,inputSize);
	
	printf("\nSecond martrix:\n");
	printMatrix(mat2,inputSize);
	
	printf("\nNormal Matrix Multiplication:\n\n");
	multresult=multiply(mat1,mat2,inputSize);
	printf("\nresult:\n\n");
	printMatrix(multresult,inputSize);
	
	int **mat_result=allocate(inputSize);
	printf("\nstrassen result:\n\n");
	strassen_multiply(mat1,mat2,mat_result,inputSize);	
	printf("\nresult:\n\n");
	printMatrix(mat_result,inputSize);	
	
	
	
		
return 0;
}
