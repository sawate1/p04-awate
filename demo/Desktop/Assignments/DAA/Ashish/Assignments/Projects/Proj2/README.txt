						CS550-04 Operating Systems
						  Programming Project 1
Ashish PramodKumar Pateria
B00638413


Status - Partially Completed but not able to Test all the test cases for scheduling.

Log of Test Cases- 
Worked- Used proj1_schdtest.c file to implement test case.

  proj1_schdtest 0- shell command to run default(round robin schedular)
  proj1_schdtest 1- shell command to run priority schedular.	

In proj1_schdtest.c file forked 2 child process with different priority.
and also printed process priority.

For default priority:
Selected Round-Robin Schedular  ...
sys_enable_sched_trace = 0
[5]Set Priority - 5  0
In parent
[5]In child
Set Priority - 6  1
[6][5][6]In child
Set Priority - 7  -1
[7][5][6][7][5][6][7][5][6][7][5][6][7][5][6][7][5][6][7][5][6][7][5][6][7][5][6][7][5][6][7][5][6][7][5][6][7][5][6][7][5][6][7][5][6][7][5][7][5][7]zombie!
zombie!


For Priority:

$ proj1_schdtest 1
[2][8][8]sys_enable_sched_trace = 0

Selected  Schedular ...
sys_enable_sched_trace = 0
[8]Set Priority - 8  0
In parent
[8]In child
Set Priority - 9  1
[9][8][9][9][9][9][9][9][9][9][9][9][9][9][9][9][9][9][8]In child
Set Priority - 10  -1
[10][8][8][8][8][8][8][8][8][8][8][8][8][8][8][8][8]zombie!
$ [10][10][10][10][10][10][10][10][10][10][10][10][10][10][10][10][10]zombie!


