#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//function definitions
void inputMatrix(int** weighted_matrix,int n);
void allocateMatrix(int** weighted_matrix,int n);
void path(int a,int b);
void floyd(int n);


int** weighted_matrix;
int** pathMatrix;

//driver function
int main(int argc,char *argv[])
{
    int n;
    printf("enter the number of vertices: ");
    scanf("%d",&n);
    
    weighted_matrix = (int**)malloc(sizeof(int*)*n);
	
    allocateMatrix(weighted_matrix,n);
  
    inputMatrix(weighted_matrix,n);
   
    floyd(n);
    
}

//Function to allocate and intialize the matrix
void allocateMatrix(int** matrix1,int n)
{
    int i,j;
	i=0;
	while(i < n)
	{
		weighted_matrix[i]=(int*)malloc(sizeof(int)*n);
		i++;
	}
    
	i=0;
	while(i < n)
	{
		for(j = 0;j < n;j++)
        	{
            		weighted_matrix[i][j]=0;
        	}
		i++;
	
	}
    
}
//Function to take input graph from text file
void inputMatrix(int** weighted_matrix,int n)
{
	FILE *fp;
   	size_t buff =0 ;
	int i,j;	
	char *token;    	
	char *line = NULL;

	//file pointer to take input file
 	fp = fopen("input.txt", "r");

	if(!fp) //check whether file exists or not.
	{
		printf("File does not exist. \n"); 
	}
	//Steps to tokenize the input  file 
	
	i=0;
	while(!feof(fp))
	{       
		getline(&line, &buff, fp);
		token = strtok(line, ",");
		int j=0;
		while(token!=NULL)
		{ 		
			weighted_matrix[i][j]=atoi(token);
			j++;
			token = strtok(NULL, ",\n"); 
		}
	
		i++;
	} 
    	printf("End of file reached \n");
    	i=0;
	while(i<n)
	{
		for (j= 0; j < n; j++)
        	{
            		printf("%d ", weighted_matrix[i][j]);
        	}
        	printf("\n");
		i++;
	}
	
}

//Function to print matrix
void printMatrix(int** weighted_matrix,int n)
{
    int i,j;
	i=0;
	while(i<n)
	{
		for(j=0;j<n;j++)
                {
                   printf("%d ",weighted_matrix[i][j]);
                }
            printf("\n");	
	    i++;
	}	
    
}

//Function to implement floyd alogorithm
void floyd(int n)
{
    	int i,j,k;
       pathMatrix = (int**)malloc(sizeof(int*)*n);
	i=0;    
	while(i < n)
	{
		pathMatrix[i]=(int*)malloc(sizeof(int)*n);
		i++;
	}

	i=0;
        while(i < n)
	{
	  	for(j = 0;j < n;j++)
        	{
            		pathMatrix[i][j]=-1;
        	}
		i++;
	}
		
    	for(i = 0;i < n;i++)
    	{
        	for(j = 0;j < n;j++)
        	{
            		pathMatrix[i][j]=-1;
        	}
    	}
  
       k=0;
	while(k < n)
	{
		i=0;
		while(i < n)
		{
			for(j = 0;j < n;j++)
            		{
                		if(weighted_matrix[i][j] > (weighted_matrix[i][k]+weighted_matrix[k][j]))
                		{
                    			weighted_matrix[i][j]=(weighted_matrix[i][k] + weighted_matrix[k][j]);
                    			pathMatrix[i][j]=k;
                		}
            		}
			i++;
		}
		k++;
	}

	printf("Input Weighted Matrix is:\n");	
        printMatrix(weighted_matrix,n);
	printf("\nPath Matrix:\n");
        printMatrix(pathMatrix,n);

	printf("\nPath with weights between each vertices:\n");
 
    i=0;
	while(i<n)
	{
		for(j=0;j<n;j++)
                {
                  printf("path = %d ->",i+1);
                  path(i,j);
                  printf("%d",j+1);
                  printf(" length= %d",weighted_matrix[i][j]);
                  printf("\n");
                }
		i++;
	}			
    
}

//function to print path between 2 vertices
void path(int x,int y)
{
	//traverse through path matrix to print the output
    if(pathMatrix[x][y]!=-1)
    {
        path(x,pathMatrix[x][y]);
        printf("%d ->",pathMatrix[x][y]+1);
        path(pathMatrix[x][y],y);
        return;
    }
    else 
        return;
}