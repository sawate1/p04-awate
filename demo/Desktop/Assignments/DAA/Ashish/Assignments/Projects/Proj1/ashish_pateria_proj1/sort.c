#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define T 1000

//function to print "*" to show array elements at each interval
void print_star(int a[],int n)
{
    int i,j;
	printf("\nIteration:::>\n");
    	for (i = 0;i < n;i++) 
        {
           	j = 0;
           	while(j < a[i])
           	{
               		printf("* ");
               		j++;

           	}
            printf("\n");
        }
}

//function to print array elements
void printList(int a[],int n)
{
    int i;
    	printf("List :: ");

    	for (i = 0;i < n;i++)
  	{
      		printf("\t%d",a[i]);
  	}

}


//function to input array elements
void input(int a[T],int n)
{
  	int i;
    	srand(time(NULL));//The pseudo-random number generator is initialized using the argument passed as seed.
    	if(n > 20)
        {
            for(i = 0;i < n;i++)
            {
              a[i] = rand(); //generates random numbers 
            }
        }
        else
        {
            for(i = 0;i < n;i++)
            {
                a[i] = rand() % 16;
            }
        }
        
}

//function insert_sort
void insertion_sort(int a[],int n)
{
  	int i,temp,k,count;
  	printf("\n\n");
    	for (i = 1;i < n;i++)
    	{
        	count=0;
		//shifting each element  to the left  until a suitable position is found for the new element.
		//a[0..i] is sorted 	
      		for (k = i; k > 0 && (a[k] < a[k-1]);k--)
       		{

           		temp = a[k];
           		a[k] = a[k-1];
                        a[k-1] = temp;
			if (n < 21)
			{					
				print_star(a,n);//to print star at each iteration
			}
			count++;
	        }

        }
        printf("\n\n\tSorted List ===>> ");
        printList(a,n);

}

//function counting_sort
void counting_sort(int n)
{
        int i,j,q,aux[100],a[T];
        srand(time(NULL));
        //random numbers between 0-99
        for(i = 0;i < n;i++)
        {
            a[i] = rand() % 100;
        }
	
	//output array elements
        for (i = 0;i < n;i++)
        {
            printf("\t%d",a[i]);
        }

    	//initializing auxiliary array
	 for (i = 0;i < 100;i++)
                aux[i] = 0;

	//Maintain the count of number of keys in an auxiliary array aux
        for (j = 0;j < n;j++)
        {
            aux[a[j]] = aux[a[j]] + 1;
        }
        
	//printing auxilary table content (skipping zero entries)	
	printf("\nAuxiliary table for Counting sort:::>\n");	
        	for(i = 0;i < 100;i++)
		{
			if(aux[i]!=0)
			{
				printf("\t%d(AUX[%d])",aux[i],i);
			}
		}
	
	//Using counts to overwrite array in place
	q = 0;
        for(j = 0;j < 100;j++)
        {
            while(aux[j] > 0)
            {
              a[q] = j;
              aux[j] = aux[j] - 1;
              q = q + 1;
            }
        }
	
	//Sorted Output 
 	   printf("\n\nSorted Elements: ");
        for (i = 0;i < n;i++)
        {
            printf("%d ",a[i]);
        }
	
	
}


//merge function for merge_sort
//merges the two sorted sub-arrays into one array
void merge(int a[T],int left,int mid,int right)
{
    int n1 = mid - left+1;
    int i,j,k;
    int n2 = right - mid;
    
    int left_temp[n1],right_temp[n2];//Temp Arrays

    // Copy data to temp arrays	
    for(i = 0;i < n1;i++)
    {
        left_temp[i] = a[left + i];

    }

    for(j = 0;j<n2;j++)
    {
        right_temp[j] = a[mid + 1 + j];
    }
    i = 0;
    j = 0;
    k = left;

    //Merge the temp arrays back into a[0..r]	
    while (i < n1 && j < n2)
    {
        if (left_temp[i] <= right_temp[j])
        {
            a[k] = left_temp[i];
            i++;
        }
        else
        {
            a[k] = right_temp[j];
            j++;
        }
        k++;
    }
    
    //Copy the remaining elements of left_temp[], if there are any 	
    while (i < n1)
    {
        a[k] = left_temp[i];
        i++;
        k++;
    }
    
    //Copy the remaining elements of right_temp[], if there are any 
    while (j < n2)
    {
        a[k] = right_temp[j];
        j++;
        k++;
    }

}

//function merge_sort
void merge_sort(int a[],int left,int right,int n)
{
     
     int mid = (left + right) / 2; //Find the middle point to divide the array into two halves
     if (left < right)
       {
		//Call mergeSort for first half             
		merge_sort(a,left,mid,n); 
	     
		//Call mergeSort for second half
		merge_sort(a,mid+1,right,n); 
	
		//Merge the two halves sorted		
		merge(a,left,mid,right);
	     	if (n < 21)
		{					
			print_star(a,n);
		}	
			
      }

}


//function to find partion element using randomization for quick sort. 
int partition(int a[],int lower,int high)
{
    int pivot,i,j,temp,t;

	t = lower + rand()%(high - lower + 1);//randomly generated pivot
	
	//pivot is replaced with the last element of aray     	
	temp = a[t];
    	a[t] = a[high];
    	a[high] = temp;

	pivot = a[high];
    	i = lower;
    	j = lower;
  

    	while(j < high)                                                // reorder the array so that all elements with values less
    	{                                                              //than the pivot come before the pivot, while all elements
								       //with values greater than the pivot come after it
        	if (a[j] <= pivot)                                                                         
        	{
            		temp = a[i];
            		a[i] = a[j];
            		a[j] = temp;
            		i++;
        	}
        	j++;

    	}
            temp = a[i];
            a[i] = a[high];
            a[high] = temp;
            return i;                  //the pivot is in its final position

}

//function quick_sort
void quick_sort(int arr[], int lower, int high,int n)
{
    int j;

    if (lower < high)
    {
        j = partition(arr,lower,high);//partition element
	if (n < 21)
	{					
		print_star(arr,n);
	}  

	/*Recursively apply the partition steps to the sub-array of elements */
   
	quick_sort(arr,lower,j-1,n);
        quick_sort(arr,j+1,high,n);
	
    }
}



// Main program to test above sorting functions //
int main()
{
    int n,arr[T],choice;
    char ch;

// Sorting Menu
     do{
        printf("\n\nSorting Menu\n\n");
        printf("1.Insertion Sort\n");
        printf("2.Counting Sort\n");
        printf("3.Merge Sort\n");
        printf("4.Quick sort\n");
        printf("5.Exit\n");
        printf("Enter your choice :: ");
        scanf("%d",&choice);
	if(choice == 5)
	{	printf("\ngoodbye\n");
                      exit(0);
	}
     	
	l1:printf("\nEnter the no of elements(1 to 1000): ");
        scanf("%d",&n);
	if( n < 1 || n > 1000)
	{
		printf("\nNo of elements should be in the range of 1 to 1000.\nPlease enter again\n");
		goto l1;
	}
	input(arr,n);

        switch (choice)
         {
          
	   case 1:    printList(arr,n);
                      insertion_sort(arr,n);
                      break;

           case 2:    counting_sort(n);
                      break;

           case 3:    printList(arr,n);
                      merge_sort(arr,0,n-1,n);
                      printf("\n\n\nSorted list===>\n");
                      printList(arr,n);
                      break;
          
	   case 4:    printList(arr,n);
		      quick_sort(arr,0,n-1,n);
                      printf("\n\n\nSorted list===>\n");
                      printList(arr,n);
                      break;

         

          default:   printf("wrong choice.Enter Again");
                     break;
          }


          getchar();
          printf("\nDo you want to continue(Y/N):\n");
          scanf("%c",&ch);


      }while(ch=='Y'||ch=='y');

  return 0;
}


